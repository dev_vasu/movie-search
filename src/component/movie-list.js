import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Avatar from "@material-ui/core/Avatar";
import Chip from "@material-ui/core/Chip";
import FaceIcon from "@material-ui/icons/Face";
import Money from "@material-ui/icons/AttachMoney";
import Language from "@material-ui/icons/Language";
import Theaters from "@material-ui/icons/Theaters";
import CalendarToday from "@material-ui/icons/CalendarToday";
import Search from "@material-ui/icons/Search";
import Star from "@material-ui/icons/Star";
import Link from "@material-ui/icons/Link";

const styles = theme => ({
  root: {
    width: "100%"
  },
  chips: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  }
});

function SimpleExpansionPanel(props) {
  const { classes } = props;
  const Items = props.movies
    .filter(movie =>
      movie.movie_title.toLowerCase().includes(props.search.toLowerCase())
    )
    .map((movie, index) => (
      <ExpansionPanel key={index}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>
            {movie.movie_title}
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.chips}>
          <Chip
            color="primary"
            label={movie.director_name}
            avatar={
              <Avatar>
                <FaceIcon />
              </Avatar>
            }
          />
          <Chip
            color="primary"
            label={movie.actor_1_name}
            avatar={
              <Avatar>
                <FaceIcon />
              </Avatar>
            }
          />
          <Chip
            color="primary"
            label={movie.actor_2_name}
            avatar={
              <Avatar>
                <FaceIcon />
              </Avatar>
            }
          />
          <Chip
            color="primary"
            label={movie.genres}
            avatar={
              <Avatar>
                <Theaters />
              </Avatar>
            }
          />
          <Chip
            color="primary"
            label={movie.language}
            avatar={
              <Avatar>
                <Language />
              </Avatar>
            }
          />
          <Chip
            color="primary"
            label={movie.country}
            avatar={
              <Avatar>
                <FaceIcon />
              </Avatar>
            }
          />
          <Chip
            color="primary"
            label={movie.content_rating}
            avatar={
              <Avatar>
                <Star />
              </Avatar>
            }
          />
          <Chip
            color="primary"
            label={movie.budget}
            avatar={
              <Avatar>
                <Money />
              </Avatar>
            }
          />
          <Chip
            color="primary"
            label={movie.title_year}
            avatar={
              <Avatar>
                <CalendarToday />
              </Avatar>
            }
          />
          <Chip
            color="primary"
            label={movie.plot_keywords}
            avatar={
              <Avatar>
                <Search />
              </Avatar>
            }
          />
          <Chip
            color="primary"
            label={movie.movie_imdb_link}
            avatar={
              <Avatar>
                <Link />
              </Avatar>
            }
          />
        </ExpansionPanelDetails>
      </ExpansionPanel>
    ));
  return <div className={classes.root}>{Items}</div>;
}

SimpleExpansionPanel.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SimpleExpansionPanel);

import React, { Component } from "react";
import axios from "axios";
import Header from "./component/header";
import SearchBar from "./component/search-bar";
import MovieList from "./component/movie-list";

class App extends Component {
  constructor(props) {
    super(props);
    this.updateSearch = this.updateSearch.bind(this);
    this.state = {
      movies: [],
      search: ""
    };
  }
  componentDidMount() {
    axios.get(`http://starlord.hackerearth.com/movieslisting`).then(result => {
      const movies = result.data;
      this.setState({
        movies: [...movies]
      });
    });
  }

  updateSearch(search) {
    this.setState({ search });
  }

  render() {
    return (
      <div>
        <Header />
        <SearchBar updateSearch={this.updateSearch} />
        <MovieList search={this.state.search} movies={this.state.movies} />
      </div>
    );
  }
}

export default App;
